<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--<%@ taglib prefix="page_tag" uri="/page_tags"%>--%>
<%
	//准备一些路径文件字符串
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() 
	                   + ":" + request.getServerPort() + path + "/";

%>

<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>图书进销存管理系统</title>
	<!-- 引入css样式文件 -->
	<!-- Bootstrap Core CSS -->
	<link href="<%=basePath%>/manager/css/bootstrap.min.css" rel="stylesheet" />
	<!-- MetisMenu CSS -->
	<link href="<%=basePath%>/manager/css/metisMenu.min.css" rel="stylesheet" />
	<!-- DataTables CSS -->
	<link href="<%=basePath%>/manager/css/dataTables.bootstrap.css" rel="stylesheet" />
	<!-- Custom CSS -->
	<link href="<%=basePath%>/manager/css/sb-admin-2.css" rel="stylesheet" />
	<!-- Custom Fonts -->
	<link href="<%=basePath%>/manager/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=basePath%>/manager/css/boot-crm.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="wrapper">
  <!-- 导航栏部分 -->
  <nav class="navbar navbar-default navbar-static-top" role="navigation"
		 style="margin-bottom: 0">
	<div class="navbar-header">
		<a class="navbar-brand" href="<%=basePath%>customer/list.action">图书进销存管理系统</a>
	</div>
	<!-- 导航栏右侧图标部分 -->
	<ul class="nav navbar-top-links navbar-right">
		<!-- 用户信息和系统设置 start -->
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#"> 
				<i class="fa fa-user fa-fw"></i>
				<i class="fa fa-caret-down"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<li><a href="#"><i class="fa fa-user fa-fw"></i>
				     用户：${manager_session.manager_name}
				    </a>
				</li>
				<li><a href="#"><i class="fa fa-gear fa-fw"></i> 修改密码</a></li>
				<li class="divider"></li>
				<li>
					<a href="${pageContext.request.contextPath }/logoutManager.action">
					<i class="fa fa-sign-out fa-fw"></i>退出登录
					</a>
				</li>
			</ul> 
		</li>
		<!-- 用户信息和系统设置结束 -->
	</ul>
	<!-- 左侧显示列表部分 start-->
	<div class="navbar-default sidebar" role="navigation">
		<div class="sidebar-nav navbar-collapse">
			<ul class=	"nav" id="side-menu">
				<li>
				    <a href="${pageContext.request.contextPath }/bookList" class="active">
				      <i class="fa fa-edit fa-fw"></i> 图书管理
				    </a>
				</li>
				<li>
				    <a href="${pageContext.request.contextPath }/orderList">
				      <i class="fa fa-dashboard fa-fw" ></i> 订单管理
				    </a>
				</li>
			</ul>
		</div>
	</div>
	<!-- 左侧显示列表部分 end--> 
  </nav>
    <!-- 图书列表查询部分  start-->
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">图书管理</h1>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<div class="panel panel-default">
			<div class="panel-body">
				<form class="form-inline" method="get" 
				      action="${pageContext.request.contextPath}/BookQuery">
<%--					隐藏的两个属性，limit和page--%>
					<input type="number" value="0" name="limit" hidden/>
					<input type="number" value="0" name="page" hidden/>
<%--					进行图书信息查询--%>
					<div class="form-group">
						<label for="bookName">图书名称</label>
						<input type="text" class="form-control" id="bookName"
						                   value="${bookName}" name="book_name" />
					</div>
					<div class="form-group">
						<label for="authorName">作者名称</label>
						<input type="text" class="form-control" id="authorName"
						                   value="${author}" name="author" />
					</div>
					<div class="form-group">
						<label for="press">出版社</label>
						<input type="text" class="form-control" id="press"
						                   value="${press}" name="press" />
					</div>
					<button type="submit" class="btn btn-primary">查询</button>
				</form>
			</div>
		</div>
<%--	数据查询的结果--%>
		<a href="#" class="btn btn-primary" data-toggle="modal" 
		           data-target="#newBookDialog" onclick="clearBook()">新建</a>
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">图书信息列表</div>
					<!-- /.panel-heading -->
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>编号</th>
								<th>图书名称</th>
								<th>作者</th>
								<th>价格</th>
								<th>出版社</th>
								<th>简介</th>
								<th>序列号</th>
                                <th>数目</th>
								<th>图片</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${book_list}" var="book">
								<tr>
									<td>${book.id}</td>
									<td>${book.book_name}</td>
									<td>${book.author}</td>
									<td>${book.price}</td>
									<td>${book.press}</td>
									<td>${book.brief_introduction}</td>
								    <td>${book.serial_number}</td>
								    <td>${book.num}</td>
									<td><img src="${book.img}" width="80" height="135" onerror="this.src='images/book.jpg'" alt="${book.book_name}"/></td>
									<td>
<%--						对图书进行删除，修改等等操作				--%>
										<a href="#" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#bookEditDialog" onclick= "editBook(${book.id})">修改</a>
										<a href="#" class="btn btn-danger btn-xs" onclick="deleteBook(${book.id})">删除</a>
										<a href="#" class="btn btn-primary btn-xs" id="new_upload_img" name="img"  data-toggle="modal" data-target="#newImgDialog" onclick="picSumbit(${book.id})">上传图片</a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<div class="col-md-12 text-right">
						<page_tag:page url="${pageContext.request.contextPath }/customer/list.action" />
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
	</div>
	<!-- 图书列表查询部分  end-->
</div>
<!-- 创建图书模态框 -->
<div class="modal fade" id="newBookDialog" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">新建图书信息</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" id="new_book_form">
					<div class="form-group">
						<label for="new_bookName" class="col-sm-2 control-label">
						    图书名称
						</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="new_bookName" placeholder="图书名称" name="book_name" />
						</div>
					</div>
					<div class="form-group">
						<label for="new_author" class="col-sm-2 control-label">作者</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="new_author" placeholder="作者" name="author" />
						</div>
					</div>
					<div class="form-group">
						<label for="new_price" class="col-sm-2 control-label">价格</label>
						<div class="col-sm-10">
							<input type="number" class="form-control" id="new_price" placeholder="价格" name="price" />
						</div>
					</div>
					<div class="form-group">
						<label for="new_press" class="col-sm-2 control-label">出版社</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="new_press" placeholder="出版社" name="press" />
						</div>
					</div>
					<div class="form-group">
						<label for="new_brief_introduction" class="col-sm-2 control-label">简介</label>
						<div class="col-sm-10">
							<input type="text" style="size: letter" class="form-control" id="new_brief_introduction" placeholder="简介" name="brief_introduction" />
						</div>
					</div>
					<div class="form-group">
						<label for="new_serial_number" class="col-sm-2 control-label">序列号</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="new_serial_number" placeholder="序列号" name="serial_number" />
						</div>
					</div>
					<div class="form-group">
						<label for="new_book_num" class="col-sm-2 control-label">数目</label>
						<div class="col-sm-10">
							<input type="number" class="form-control" id="new_book_num" placeholder="数目" name="storage.num" />
						</div>
					</div>
<%--                    图书的img进行hidden，方便数据的传输--%>
                    <input type="hidden" id="new_img" name="img"/>
				</form>
<%--				<div class="form-group">-
-%>
<%--					<div class="col-lg-4">--%>
<%--						<button  type="button" class="btn bg-primary" id="new_upload_img" name="img"  data-toggle="modal" data-target="#newImgDialog" onclick="checkPic()" >上传图片</button>--%>
<%--					</div>--%>
<%--				</div>--%>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" onclick="createBook()">创建图书</button>
			</div>
		</div>
	</div>
</div>
<!-- 修改图书模态框 -->
<div class="modal fade" id="bookEditDialog" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="EditBookModalLabel">修改图书信息</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" id="edit_book_form">
					<input type="hidden" id="edit_book_id" name="id"/>
					<div class="form-group">
						<label for="edit_BookName" class="col-sm-2 control-label">图书名称</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="edit_BookName" placeholder="图书名称" name="book_name" />
						</div>
					</div>
					<div class="form-group">
                        <label for="edit_author" class="col-sm-2 control-label">作者</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="edit_author" placeholder="作者" name="author" />
                        </div>
					</div>
                    <div class="form-group">
                        <label for="edit_price" class="col-sm-2 control-label">价格</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" id="edit_price" placeholder="价格" name="price" />
                        </div>
                    </div>
					<div class="form-group">
                        <label for="edit_press" class="col-sm-2 control-label">出版社</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="edit_press" placeholder="出版社" name="press" />
                        </div>
					</div>
					<div class="form-group">
						<label for="edit_brief_introduction" class="col-sm-2 control-label">简介</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="edit_brief_introduction" placeholder="简介" name="brief_introduction" />
						</div>
					</div>
					<div class="form-group">
						<label for="edit_serial_number" class="col-sm-2 control-label">序列号</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="edit_serial_number" placeholder="序列号" name="serial_number" />
						</div>
					</div>
                    <div class="form-group">
                        <label for="edit_book_number" class="col-sm-2 control-label">数目</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="edit_book_number" placeholder="数目" name="storage.num" />
                        </div>
                    </div>
				</form>
<%--				<div class="form-group">--%>
<%--					<div class="col-lg-4">--%>
<%--						<button  type="button" class="btn bg-primary" id="edit_img" name="img"  data-toggle="modal" data-target="#newImgDialog">修改图片 </button>--%>
<%--					</div>--%>
<%--				</div>--%>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" onclick="updateBook()">保存修改</button>
			</div>
		</div>
	</div>
</div>
<%--创建一个图片上传的form--%>
<div class="modal fade" id="newImgDialog" tabindex="-1" role="dialog"
	 aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="graphics-document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="BookModalLabel">上传图书的图片</h4>
			</div>
<%--			上传图书的表单设计--%>
			<div class="modal-body">
				<form class="form-horizontal" id="new_img_form"  method = "post" enctype="multipart/form-data" action="<%=basePath%>pictureUpload" onsubmit="picSumbit()">
					<input type="text"  name="name" id="BookName" hidden>
					<div class="form-group">
						<label for="picture" class="col-sm-2 control-label">上传图片</label>
						<div class="col-lg-1">
							<input id="picture" type="file" name="uploadPic"/>
						</div>
					</div>
				<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="submit" class="btn bg-primary" id="confirmUpload_img" >确认</button>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- 引入js文件 -->
<!-- jQuery -->
<script src="<%=basePath%>/manager/js/jquery-1.11.3.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<%=basePath%>/manager/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<%=basePath%>/manager/js/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<%=basePath%>/manager/js/jquery.dataTables.min.js"></script>
<script src="<%=basePath%>/manager/js/dataTables.bootstrap.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<%=basePath%>/manager/js/sb-admin-2.js"></script>


<!-- 编写js代码 -->
<script type="text/javascript">
	//还是处理图片的上传函数
	function picSumbit(id) {
		$.ajax({
			type:"get",
			url:"<%=basePath%>book/getBookById",
			data:{"id":id},
			success:function(data) {
				$("#BookName").val(data.book_name);
			}
		});
	}
//清空新建图书窗口中的数据
	function clearBook() {
	    $("#new_bookName").val("");
	    $("#new_author").val("");
	    $("#new_price").val("");
	    $("#new_press").val("");
	    $("#new_brief_introduction").val("");
	    $("#new_serial_number").val("");
	    $("#new_img").val("");
	}

	// 创建图书
	function createBook() {
	$("#new_img").val("images/books/"+$("#new_bookName").val()+".jpg");
	$.post("<%=basePath%>book/create",
			$("#new_book_form").serialize(),function(data){
	        if(data == "OK"){
	            alert("图书创建成功！");
	            window.location.reload();
	        }else{
	            alert("图书创建失败！");
	            window.location.reload();
	        }
	    });
	}

	// 通过id获取修改的图书信息
	function editBook(id) {
	    $.ajax({
	        type:"get",
	        url:"<%=basePath%>book/getBookById",
	        data:{"id":id},
	        success:function(data) {
	            $("#edit_book_id").val(data.id);
	            $("#edit_BookName").val(data.book_name);
	            $("#edit_author").val(data.author)
	            $("#edit_price").val(data.price)
	            $("#edit_press").val(data.press)
	            $("#edit_brief_introduction").val(data.brief_introduction)
	            $("#edit_serial_number").val(data.serial_number);
	            $("#edit_book_number").val(data.storage.num);
	        }
	    });
	}

    // 执行修改图书操作
	function updateBook() {
		$.post("<%=basePath%>book/update",$("#edit_book_form").serialize(),function(data){
			if(data =="OK"){
				alert("图书信息更新成功！");
				window.location.reload();
			}else{
				alert("图书信息更新失败！");
				window.location.reload();
			}
		});
	}
	// 删除图书
	function deleteBook(id) {
		if(confirm('确实要删除该图书吗?')) {
	$.post("<%=basePath%>book/delete",{"id":id},
	function(data){
	            if(data =="OK"){
	                alert("图书删除成功！");
	                window.location.reload();
	            }else{
	                alert("删除图书失败！");
	                window.location.reload();
	            }
	        });
	    }
	}
</script>
</body>

</html>