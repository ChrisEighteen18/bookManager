<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--<%@ taglib prefix="page_tag" uri="/page_tags"%>--%>
<%
	//准备一些路径文件字符串
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() 
	                   + ":" + request.getServerPort() + path + "/";

%>

<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>图书进销存管理系统</title>
	<!-- 引入css样式文件 -->
	<!-- Bootstrap Core CSS -->
	<link href="<%=basePath%>/manager/css/bootstrap.min.css" rel="stylesheet" />
	<!-- MetisMenu CSS -->
	<link href="<%=basePath%>/manager/css/metisMenu.min.css" rel="stylesheet" />
	<!-- DataTables CSS -->
	<link href="<%=basePath%>/manager/css/dataTables.bootstrap.css" rel="stylesheet" />
	<!-- Custom CSS -->
	<link href="<%=basePath%>/manager/css/sb-admin-2.css" rel="stylesheet" />
	<!-- Custom Fonts -->
	<link href="<%=basePath%>/manager/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=basePath%>/manager/css/boot-crm.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="wrapper">
  <!-- 导航栏部分 -->
  <nav class="navbar navbar-default navbar-static-top" role="navigation"
		 style="margin-bottom: 0">
	<div class="navbar-header">
		<a class="navbar-brand" href="<%=basePath%>customer/list.action">图书进销存管理系统</a>
	</div>
	<!-- 导航栏右侧图标部分 -->
	<ul class="nav navbar-top-links navbar-right">
		<!-- 用户信息和系统设置 start -->
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#"> 
				<i class="fa fa-user fa-fw"></i>
				<i class="fa fa-caret-down"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<li><a href="#"><i class="fa fa-user fa-fw"></i>
				     用户：${USER_SESSION.user_name}
				    </a>
				</li>
				<li><a href="#"><i class="fa fa-gear fa-fw"></i> 修改密码</a></li>
				<li class="divider"></li>
				<li>
					<a href="${pageContext.request.contextPath }/logoutUser.action">
					<i class="fa fa-sign-out fa-fw"></i>退出登录
					</a>
				</li>
			</ul> 
		</li>
		<!-- 用户信息和系统设置结束 -->
	</ul>
	<!-- 左侧显示列表部分 start-->
	<div class="navbar-default sidebar" role="navigation">
		<div class="sidebar-nav navbar-collapse">
			<ul class=	"nav" id="side-menu">
				<li>
				    <a href="${pageContext.request.contextPath }/bookList" >
				      <i class="fa fa-edit fa-fw"></i> 图书查看
				    </a>
				</li>
				<li>
				    <a href="${pageContext.request.contextPath }/orderList" class="active">
				      <i class="fa fa-dashboard fa-fw" ></i> 订单查看
				    </a>
				</li>
			</ul>
		</div>
	</div>
	<!-- 左侧显示列表部分 end--> 
  </nav>
    <!-- 图书列表查询部分  start-->
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">查看订单</h1>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
<%--	数据查询的结果--%>
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default	">
					<div class="panel-heading">订单列表</div>
					<!-- /.panel-heading -->
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>编号</th>
								<th>用户名字</th>
								<th>书名</th>
								<th>已购数目</th>
								<th>图片</th>
								<th>订单情况</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${order_list}"  var="order">
								<tr>
									<td>${order.id}</td>
									<td>${order.user_name}</td>
									<td>${order.book_name}</td>
									<td>${order.num}</td>
									<td><img src="${order.img}" width="80" height="135" onerror="this.src='images/book.jpg'" alt="${order.book.book_name}"/></td>
									<td>
										<c:if test="${order.situation==0}">
										暂未处理订单
										</c:if>
										<c:if test="${order.situation==1}">
										订单已处理
										</c:if>
									</td>
									<td>
										<a class="btn btn-primary btn-xs"  onclick= "deleteOrder(${order.id})">删除</a>
										<a class="btn btn-danger btn-xs"  onclick= "updateOrder(${order.id})">处理</a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
	</div>
	<!-- 图书列表查询部分  end-->
</div>

<!-- 引入js文件 -->
<!-- jQuery -->
<script src="<%=basePath%>/manager/js/jquery-1.11.3.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<%=basePath%>/manager/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<%=basePath%>/manager/js/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<%=basePath%>/manager/js/jquery.dataTables.min.js"></script>
<script src="<%=basePath%>/manager/js/dataTables.bootstrap.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<%=basePath%>/manager/js/sb-admin-2.js"></script>


<!-- 编写js代码 -->
<script type="text/javascript">
    // 执行
	function updateOrder(id) {
        if(confirm('确实要处理该订单吗?')){
            $.post("<%=basePath%>order/update",{"id":id},function(data){
                if(data == "OK"){
                    alert("订单处理成功！");
                    window.location.reload();
                }else{
                    alert("订单处理失败！ 或者订单已经处理了～");
					window.location.reload();
                }
            });
        }
	}
	// 删除图书
	function deleteOrder(id) {
		if(confirm('确实要删除该图书吗?')) {
	$.post("<%=basePath%>order/delete",{"id":id},
	function(data){
	            if(data =="OK"){
	                alert("图书删除成功！");
	                window.location.reload();
	            }else{
	                alert("删除图书失败！");
	                window.location.reload();
	            }
	        });
	    }
	}
</script>
</body>

</html>