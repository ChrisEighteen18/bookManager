<%--
  Created by IntelliJ IDEA.
  User: Chris
  Date: 16/06/2021
  Time: 10:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Title</title>
</head>
<body>
<%
    //准备一些路径文件字符串
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName()
            + ":" + request.getServerPort() + path ;

%>
<%--<script type="application/javascript">--%>
<%--    //图片上传的验证代码--%>
<%--    function checkPic() {--%>
<%--        &lt;%&ndash;$.post("<%=basePath%>pictureUpload",&ndash;%&gt;--%>
<%--        &lt;%&ndash;		$("#new_img_form").serialize(),function(data){&ndash;%&gt;--%>
<%--        &lt;%&ndash;			if(data == "OK"){&ndash;%&gt;--%>
<%--        &lt;%&ndash;				alert("上传成功！");&ndash;%&gt;--%>
<%--        &lt;%&ndash;			}else{&ndash;%&gt;--%>
<%--        &lt;%&ndash;				alert("上传失败！");&ndash;%&gt;--%>
<%--        &lt;%&ndash;			}&ndash;%&gt;--%>
<%--        &lt;%&ndash;		});&ndash;%&gt;--%>
<%--        $.ajax({--%>
<%--            url:"<%=basePath%>pictureUpload",--%>
<%--            type: "POST",--%>
<%--            data:$("#new_img_form").serialize(),--%>
<%--            cache:false,--%>
<%--            contentType:false--%>
<%--        });--%>
<%--    }--%>
<%-- </script>--%>
<form class="form-horizontal" id="new_img_form"  method = "post" enctype="multipart/form-data" action="<%=basePath%>/pictureUpload">
    <input type="text" hidden name="name"  id="name">
    <div class="form-group">
        <label for="picture" class="col-sm-2 control-label">上传图片</label>
        <div class="col-lg-1">
            <input id="picture" type="file" name="uploadPic" >
        </div>
        <button type="submit" class="btn bg-primary" id="confirmUpload_img">确认</button>
    </div>
</form>
</body>
</html>
