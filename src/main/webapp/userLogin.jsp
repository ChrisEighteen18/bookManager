<%@ page import="utils.SessionString" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!doctype html>
<html lang="zh">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.0.1">
    <title>欢迎使用Chris图书系统</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/sign-in/">

    <!-- Bootstrap core CSS -->
<link href="css/bootstrap.css" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">
    <script type="javascript">
        // 判断是登录账号和密码是否为空
        function check(){
            var username = $("#username").val();
            var password = $("#password").val();
            if(username=="" || password==""){
                $("#message").text("账号或密码不能为空！");
                return false;
            }
            return true;
        }
    </script>
  </head>
  <%
    String msg = (String) session.getAttribute(SessionString.MSG);
    if(msg == null){
        msg = "欢迎使用由Chris设计的图书系统~";
    }
  %>
  <body class="text-center">
    <
    <form class="form-signin" method="post" onsubmit="check()" action="${pageContext.request.contextPath}/userLogin.action">
      <img class="mb-4" src="images/book.jpg" alt="" width="72" height="72">
      <h1 class="h3 mb-3 font-weight-normal">图书管理系统-普通用户
      </h1>
<%--      <label for="inputEmail" class="sr-only">用户名</label>--%>
      <input type="text"  id="username" name="username" class="form-control" placeholder="UserName" required autofocus>
<%--      <label for="inputPassword" class="sr-only">密码</label>--%>
      <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
       <label class="col-form-label" id="msg"><%=msg%></label>
       <label><a href="userRegister.jsp">我还没有账号，需要注册~</a><br></label>
      <button class="btn btn-lg btn-primary btn-block" type="submit">登录</button>
      <a href="managerLogin.jsp">管理者界面</a>
      <p class="mt-5 mb-3 text-muted">&copy; 2017-2021</p>
    </form>
</body>
</html>
