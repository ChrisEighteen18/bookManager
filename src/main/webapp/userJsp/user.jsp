<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--<%@ taglib prefix="page_tag" uri="/page_tags"%>--%>
<%
	//准备一些路径文件字符串
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() 
	                   + ":" + request.getServerPort() + path + "/";

%>

<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>图书进销存管理系统</title>
	<!-- 引入css样式文件 -->
	<!-- Bootstrap Core CSS -->
	<link href="<%=basePath%>/manager/css/bootstrap.min.css" rel="stylesheet" />
	<!-- MetisMenu CSS -->
	<link href="<%=basePath%>/manager/css/metisMenu.min.css" rel="stylesheet" />
	<!-- DataTables CSS -->
	<link href="<%=basePath%>/manager/css/dataTables.bootstrap.css" rel="stylesheet" />
	<!-- Custom CSS -->
	<link href="<%=basePath%>/manager/css/sb-admin-2.css" rel="stylesheet" />
	<!-- Custom Fonts -->
	<link href="<%=basePath%>/manager/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=basePath%>/manager/css/boot-crm.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="wrapper">
  <!-- 导航栏部分 -->
  <nav class="navbar navbar-default navbar-static-top" role="navigation"
		 style="margin-bottom: 0">
	<div class="navbar-header">
		<a class="navbar-brand" href="<%=basePath%>/bookList">图书进销存管理系统</a>
	</div>
	<!-- 导航栏右侧图标部分 -->
	<ul class="nav navbar-top-links navbar-right">
		<!-- 用户信息和系统设置 start -->
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#"> 
				<i class="fa fa-user fa-fw"></i>
				<i class="fa fa-caret-down"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<li><a href="#"><i class="fa fa-user fa-fw"></i>
				     用户：${USER_SESSION.user_name}
				    </a>
				</li>
				<li><a href="#"><i class="fa fa-gear fa-fw"></i> 修改密码</a></li>
				<li class="divider"></li>
				<li>
					<a href="${pageContext.request.contextPath }/logoutUser.action">
					<i class="fa fa-sign-out fa-fw"></i>退出登录
					</a>
				</li>
			</ul> 
		</li>
		<!-- 用户信息和系统设置结束 -->
	</ul>
	<!-- 左侧显示列表部分 start-->
	<div class="navbar-default sidebar" role="navigation">
		<div class="sidebar-nav navbar-collapse">
			<ul class=	"nav" id="side-menu">
				<li>
				    <a href="${pageContext.request.contextPath }/bookList" class="active">
				      <i class="fa fa-edit fa-fw"></i> 图书查看
				    </a>
				</li>
				<li>
				    <a href="${pageContext.request.contextPath }/orderList">
				      <i class="fa fa-dashboard fa-fw" ></i> 订单查看
				    </a>
				</li>
			</ul>
		</div>
	</div>
	<!-- 左侧显示列表部分 end--> 
  </nav>
    <!-- 图书列表查询部分  start-->
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">查看图书</h1>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<div class="panel panel-default">
			<div class="panel-body">
				<form class="form-inline" method="get" 
				      action="${pageContext.request.contextPath}/BookQuery">
<%--					隐藏的两个属性，limit和page--%>
					<input type="number" value="0" name="limit" hidden/>
					<input type="number" value="0" name="page" hidden/>
<%--					进行图书信息查询--%>
					<div class="form-group">
						<label for="bookName">图书名称</label>
						<input type="text" class="form-control" id="bookName"
						                   value="${bookName}" name="book_name" />
					</div>
					<div class="form-group">
						<label for="authorName">作者名称</label>
						<input type="text" class="form-control" id="authorName"
						                   value="${author}" name="author" />
					</div>
					<div class="form-group">
						<label for="press">出版社</label>
						<input type="text" class="form-control" id="press"
						                   value="${press}" name="press" />
					</div>
					<button type="submit" class="btn btn-primary">查询</button>
				</form>
			</div>
		</div>
<%--	数据查询的结果--%>
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">图书信息列表</div>
					<!-- /.panel-heading -->
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>编号</th>
								<th>图书名称</th>
								<th>作者</th>
								<th>价格</th>
								<th>出版社</th>
								<th>简介</th>
								<th>序列号</th>
                                <th>数目</th>
								<th>图片</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${book_list}" var="book">
								<tr>
									<td>${book.id}</td>
									<td>${book.book_name}</td>
									<td>${book.author}</td>
									<td>${book.price}</td>
									<td>${book.press}</td>
									<td>${book.brief_introduction}</td>
								    <td>${book.serial_number}</td>
								    <td>${book.num}</td>
									<td><img src="${book.img}" width="80" height="135" onerror="this.src='images/book.jpg'" alt="${book.book_name}"/></td>
									<td>
										<a href="#" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#newOrderDialog" onclick= "chooseBook(${book.id})">下订单</a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
	</div>
	<!-- 图书列表查询部分  end-->
</div>
<%--创建一个下订单的form--%>
<div class="modal fade" id="newOrderDialog" tabindex="-1" role="dialog"
	 aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="graphics-document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="BookModalLabel">上传图书的图片</h4>
			</div>
<%--			上传订单的表单设计--%>
			<div class="modal-body">
                <form class="form-horizontal" id="choose_book_form" >
						<div class="form-group">
							<label for="choose_BookName" class="col-sm-2 control-label">图书名称：</label>
							<div class="col-lg-1">
								<input type="text"  name="book_name" id="choose_BookName" disabled/>
							</div>
						</div>
						<div class="form-group">
							<label for="choose_book_number" class="col-sm-2 control-label">数目：</label>
							<div class="col-lg-1">
								<input type="text"   id="choose_book_number" disabled/>
							</div>
						</div>

<%--				这里才是表单--%>

					<div class="form-group">
						<div class="col-lg-1">
							<input type="number"  name="storage.id" id="choose_book_id" hidden/>
						</div>
					</div>
					<div class="form-group">
						<label for="choose_buy_number" class="col-sm-2 control-label">购买数目：</label>
						<div class="col-lg-1">
							<input type="number"  name="num" id="choose_buy_number" />
						</div>
					</div>
				<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="submit" class="btn bg-primary" onclick="insertOrder()">确认</button>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- 引入js文件 -->
<!-- jQuery -->
<script src="<%=basePath%>/manager/js/jquery-1.11.3.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<%=basePath%>/manager/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<%=basePath%>/manager/js/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<%=basePath%>/manager/js/jquery.dataTables.min.js"></script>
<script src="<%=basePath%>/manager/js/dataTables.bootstrap.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<%=basePath%>/manager/js/sb-admin-2.js"></script>


<!-- 编写js代码 -->
<script type="text/javascript">
	// 通过id获取的图书信息并且选择图书
	function chooseBook(id) {
	    $.ajax({
	        type:"get",
	        url:"<%=basePath%>book/getBookById",
	        data:{"id":id},
	        success:function(data) {
	            $("#choose_book_id").val(data.id);
	            $("#choose_BookName").val(data.book_name);
	            $("#choose_author").val(data.author)
	            $("#choose_price").val(data.price)
	            $("#choose_press").val(data.press)
	            $("#choose_brief_introduction").val(data.brief_introduction)
	            $("#choose_serial_number").val(data.serial_number);
	            $("#choose_book_number").val(data.storage.num);
	        }
	    });
	}

    // 执行
	function insertOrder() {
        if(confirm('确实要购买该图书吗?')){
            $.post("<%=basePath%>/newOrder",
                $("#choose_book_form").serialize(),
                function(data){
                if(data == "OK"){
                    alert("图书购买成功！");
                    window.location.reload();
                }else{
                    alert("图书购买失败！");
					window.location.reload();
                }
            });
        }
	}
	// 删除图书
	function deleteBook(id) {
		if(confirm('确实要删除该图书吗?')) {
	$.post("<%=basePath%>book/delete",{"id":id},
	function(data){
	            if(data =="OK"){
	                alert("图书删除成功！");
	                window.location.reload();
	            }else{
	                alert("删除图书失败！");
	                window.location.reload();
	            }
	        });
	    }
	}
</script>
</body>

</html>