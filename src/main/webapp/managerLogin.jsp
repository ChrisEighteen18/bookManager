<%@ page import="utils.SessionString" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!doctype html>
<html lang="zh">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.0.1">
    <title>管理者登录界面</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/sign-in/">

    <!-- Bootstrap core CSS -->
<link href="css/bootstrap.css" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">
  </head>
  <body class="text-center">
  <%
      String msg = (String) session.getAttribute(SessionString.MSG);
      if(msg == null){
          msg = "欢迎使用由Chris设计的图书系统~";
      }
  %>
    <form class="form-signin" method="post" action="${pageContext.request.contextPath}/loginManager.action">
      <img class="mb-4" src="images/book.jpg" alt="" width="72" height="72">
      <h1 class="h3 mb-3 font-weight-normal">图书管理系统-管理者</h1>
<%--      <label for="inputEmail" class="sr-only">用户名</label>--%>
      <input type="text" id="manager_name" class="form-control"  name="manager_name" placeholder="ManagerName" required autofocus>
<%--      <label for="inputPassword" class="sr-only">密码</label>--%>
      <input type="password" id="manager_password" class="form-control" name = "manager_password" placeholder="Password" required>
<%--      <div class="checkbox mb-3">--%>
<%--        <label>--%>
<%--          <input type="checkbox" value="remember-me"> Remember me--%>
<%--        </label>--%>
<%--      </div>--%>
        <label><h1 class="col-form-label"><%=msg%></h1> </label>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      <a href="index.jsp">返回用户登录界面</a>
      <p class="mt-5 mb-3 text-muted">&copy; 2017-2021</p>
    </form>
</body>
</html>
