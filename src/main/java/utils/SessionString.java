package utils;

public class SessionString {
    public static final String USER_SESSION ="USER_SESSION";
    public static final String MSG ="msg";
    public static final String Manager_SESSION ="manager_session";
    public static final String BOOK_LIST = "book_list";
    public static final String ORDER_LIST = "order_list";
}
