package utils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeStampFactory {
    public static Timestamp getSystemTime()

    {
        Date dt = new Date();

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String nowTime = df.format(dt);

        Timestamp timestamp = Timestamp.valueOf(nowTime);

        return timestamp;

    }
}

