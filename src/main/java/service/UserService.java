package service;

import org.apache.ibatis.annotations.Param;
import po.User;

import java.util.List;

public interface UserService {
    //读取数据库中所有的User
    public List<User> findAllUser();
    //通过用户和密码来查询User
    public User findUserByNameAndPassword(String username,String password);
    //添加一个新用户
    public int addNewUser(User user);

}
