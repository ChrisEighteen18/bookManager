package service.impl;

import dao.StorageDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import po.Storage;
import service.StorageService;
@Service("StorageService")
public class StorageServiceImpl implements StorageService {
    @Autowired
    private StorageDao storageDao;
    @Override
    public Storage findStorageById(Integer id) {
        return this.storageDao.findStorageById(id);
    }

    @Override
    public int updateStorageNum(Storage storage) {
        return this.storageDao.updateStorageNum(storage);
    }

    @Override
    public int insertStorage(Storage storage) {
        return this.storageDao.insertStorage(storage);
    }
}
