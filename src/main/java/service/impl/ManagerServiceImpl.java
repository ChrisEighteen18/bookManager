package service.impl;

import dao.ManagerDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import po.Manager;
import service.ManagerService;
@Service("ManagerService")
@Transactional
public class ManagerServiceImpl implements ManagerService {
    @Autowired
    private ManagerDao managerDao;
    @Override
    public Manager findManagerByNameAndPassword(String manager_name, String manager_password) {
        return this.managerDao.findManagerByNameAndPassword(manager_name,manager_password);
    }
}
