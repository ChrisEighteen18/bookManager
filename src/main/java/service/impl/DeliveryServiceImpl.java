package service.impl;

import dao.DeliveryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import po.Delivery;
import service.DeliveryService;
@Service("DeliveryService")
public class DeliveryServiceImpl implements DeliveryService {
    @Autowired
    private DeliveryDao deliveryDao;
    @Override
    public int insertDelivery(Delivery delivery) {
        return this.deliveryDao.insertDelivery(delivery);
    }

    @Override
    public Delivery selectDelivery(Delivery delivery) {
        return this.deliveryDao.selectDelivery(delivery);
    }
}
