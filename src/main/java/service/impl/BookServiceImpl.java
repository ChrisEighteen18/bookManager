package service.impl;

import com.github.pagehelper.PageHelper;
import dao.BookDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import po.Book;
import service.BookService;

import java.util.List;
import java.util.Map;

@Service("BookService")
public class  BookServiceImpl implements BookService {
    @Autowired
    private BookDao bookDao;
    //在Service的Impl中调用插件进行后端持久层的应用
    @Override
    public List<Map<String, Object>> selectAllBook(Map<String, Object> map) {
        return this.bookDao.selectAllBook(map);
    }

    @Override
    public List<Map<String, Object>> selectBookPageWhere(Map<String, Object> map) {
        return this.bookDao.selectBookPageWhere(map);
    }

    @Override
    public Book selectBookById(int id) {
        return this.bookDao.selectBookById(id);
    }

    @Override
    public int updateBook(Book book) {
        return this.bookDao.updateBook(book);
    }

    @Override
    public int deleteBook(int id) {
        return this.bookDao.deleteBook(id);
    }

    @Override
    public int insertBook(Book book) {
        return this.bookDao.insertBook(book);
    }

    @Override
    public Book selectBookByBook(Book book) {
        return this.bookDao.selectBookByBook(book);
    }

    @Override
    public int updateBookStorageId(Book book) {
        return this.bookDao.updateBookStorageId(book);
    }
}
