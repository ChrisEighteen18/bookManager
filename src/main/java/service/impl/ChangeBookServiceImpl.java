package service.impl;

import dao.ChangeBookDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import po.ChangeBook;
import service.ChangeBookService;
@Service("ChangeBookService")
public class ChangeBookServiceImpl implements ChangeBookService {
    @Autowired
    private ChangeBookDao changeBookDao;
    @Override
    public int insertChange(ChangeBook changeBook) {
        return this.changeBookDao.insertChange(changeBook);
    }
}
