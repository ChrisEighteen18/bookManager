package service.impl;

import dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import po.User;
import service.UserService;


import java.util.List;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService{
    @Autowired
    private UserDao userDao;
    @Override
    public List<User> findAllUser() {
        return this.userDao.findAllUser();
    }

    @Override
    public User findUserByNameAndPassword(String username, String password) {
        return this.userDao.findUserByNameAndPassword(username,password);
    }

    @Override
    public int addNewUser(User user ) {
        return this.userDao.addNewUser(user);
    }
}
