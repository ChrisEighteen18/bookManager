package service.impl;

import dao.OrderDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import po.Order;
import service.OrderService;

import java.util.List;
import java.util.Map;
@Service("OrderService")
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderDao orderDao;
    @Override
    public List<Map<String, Object>> selectAllOrder(Map<String, Object> map) {
        return this.orderDao.selectAllOrder(map);
    }

    @Override
    public List<Map<String, Object>> selectOrderPageWhere(Map<String, Object> map) {
        return this.orderDao.selectOrderPageWhere(map);
    }

    @Override
    public int insertOneOrder(Order order) {
        return this.orderDao.insertOneOrder(order);
    }

    @Override
    public int deleteOrder(Integer id) {
        return this.orderDao.deleteOrder(id);
    }

    @Override
    public int updateOrder(Integer id) {
        return this.orderDao.updateOrder(id);
    }

    @Override
    public int updateOrderAll(Order order) {
        return this.orderDao.updateOrderAll(order);
    }
}
