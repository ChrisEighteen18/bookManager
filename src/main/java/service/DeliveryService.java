package service;

import po.Delivery;

public interface DeliveryService {
    //主要是进行添加
    public int insertDelivery(Delivery delivery);
    //获取原delivery
    public Delivery selectDelivery(Delivery delivery);
}
