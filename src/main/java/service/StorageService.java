package service;

import po.Storage;

public interface StorageService {
    //查找该图书的数目
    public Storage findStorageById(Integer id);
    //更新图书库存包括id和数目
    public int updateStorageNum(Storage storage);
    //增加库存
    public int insertStorage(Storage storage);
}
