package service;

import org.apache.ibatis.annotations.Param;
import po.Manager;

public interface ManagerService {
    //查询是否由该管理者
    public Manager findManagerByNameAndPassword( String manager_name,String manager_password);

}
