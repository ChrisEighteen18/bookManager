package service;

import po.Order;

import java.util.List;
import java.util.Map;

public interface OrderService {
    //查询所有的订单
    public List<Map<String,Object>> selectAllOrder(Map<String ,Object>map);
    //仅展示该用户下的所有订单
    public List<Map<String,Object>> selectOrderPageWhere(Map<String ,Object>map);
    //增加一个订单
    public int insertOneOrder(Order order);
    //删除一个订单
    public int deleteOrder(Integer id);
    //更动订单--主要就是更动管理者确认与否
    public int updateOrder(Integer id);
    //更动订单--主要就是更动管理者确认与否
    public int updateOrderAll(Order order);
}
