package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import po.Manager;
import service.ManagerService;
import utils.SessionString;

import javax.servlet.http.HttpSession;

@Controller
public class ManagerController {
    @Autowired
    private ManagerService managerService;
    @RequestMapping(value = "/loginManager.action",method = RequestMethod.POST)
    public String login(String manager_name, String  manager_password, HttpSession httpSession) {
        Manager manager = managerService.findManagerByNameAndPassword(manager_name,manager_password);
        if(manager != null){
            httpSession.setAttribute(SessionString.Manager_SESSION,manager);
            return "redirect:/BookAll";//跳转到管理者界面。
        }else{
            httpSession.setAttribute(SessionString.MSG,"您的管理者的登录信息有误，请重新输入~");
            return "managerLogin";
        }
    }
    //使用get方法进行登录
    @RequestMapping(value = "/loginManager.action" ,method = RequestMethod.GET)
    public String toLogin() {
        return "managerLogin";
    }

    //管理者进行退出登录
    @RequestMapping("/logoutManager.action")
    public String logOut(HttpSession httpSession){
        httpSession.invalidate();
        return toLogin();
    }

    //图书列表跳转
    @RequestMapping(value =  "/bookList",method = RequestMethod.GET)
    public String showBookList(){
        return "forward:/BookAll";
    }
}
