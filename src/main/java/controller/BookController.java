package controller;

import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import po.*;
import service.BookService;
import service.ChangeBookService;
import service.StorageService;
import utils.BookChangeStuation;
import utils.SessionString;

import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//这个对象是用来进行分页的
@Controller
public class BookController {
    @Autowired
    private BookService bookService;
    @Autowired
    private StorageService storageService;//增加库存
    //对于图书的管理
    @Autowired
    private ChangeBookService changeBookService;
    //通过查询获取部分的相应图书
    @RequestMapping(value = "/BookQuery")
    public String getSomeBooks(@RequestParam Map<String,Object> map, HttpSession httpSession){
        //注意两个分页的必要参数，page和limit
        List<Map<String,Object>> list = bookService.selectBookPageWhere(map);
        PageInfo<Map<String ,Object>> pageInfo = new PageInfo<Map<String,Object>>();
        map.put("list",list);
        //聚合数目最大值
        map.put("total",pageInfo.getTotal());
        httpSession.setAttribute(SessionString.BOOK_LIST,list);
        //通过Session进行判定，如果是user那就跳转到user的界面，反之就是manager的界面
        Manager manager = (Manager) httpSession.getAttribute(SessionString.Manager_SESSION);
        if(manager != null){
            return "/managerJsp/manager";
        }else{
            return "/userJsp/user";
        }
    }
    //通过查询获取全部图书
    @RequestMapping(value = "/BookAll")
    public String getAllBooks(@RequestParam Map<String,Object> map, HttpSession httpSession){
        //注意两个分页的必要参数，page和limit
        List<Map<String,Object>> list = bookService.selectAllBook(map);
        PageInfo<Map<String ,Object>> pageInfo = new PageInfo<Map<String,Object>>();
        map.put("list",list);
        //设置page和limit
        map.put("page",0);
        map.put("limit",0);
        //聚合数目最大值
        map.put("total",pageInfo.getTotal());
        httpSession.setAttribute(SessionString.BOOK_LIST,list);
        //通过Session进行判定，如果是user那就跳转到user的界面，反之就是manager的界面
        Manager manager = (Manager) httpSession.getAttribute(SessionString.Manager_SESSION);
        if(manager != null){
            return "/managerJsp/manager";
        }else{
            return "/userJsp/user";
        }
    }

    //通过id获取图书信息
    @RequestMapping("/book/getBookById")
    @ResponseBody
    public Book getBookById(Integer id){
        Book book = bookService.selectBookById(id);
        return book;
    }

    //更新图书
    @RequestMapping("/book/update")
    @ResponseBody
    public String updateBook(Book book,HttpSession session){
        //增加
        Book book1 = bookService.selectBookById(book.getId());
        boolean flag = changeBook(BookChangeStuation.UPDATE_BOOK,session,book);
        int rows = bookService.updateBook(book);
        if(rows > 0 && flag ){
            return "OK";
        }else{
            return "FAIL";
        }
    }

    //删除图书
    @RequestMapping("book/delete")
    @ResponseBody
    public String deleteBook(Integer id,HttpSession session){
        Book book = bookService.selectBookById(id);
        boolean flag = changeBook(BookChangeStuation.DELETE_BOOK,session,book);
        int rows = bookService.deleteBook(id);
        if(rows > 0 && flag){
            return "OK";
        }else{
            return "FAIL";
        }
    }

    //增加图书
    @RequestMapping("/book/create")
    @ResponseBody
    public String createBook(Book book){
        int num = book.getStorage().getNum();
        Storage storage  = new Storage();
        storage.setNum(num);
       int row = storageService.insertStorage(storage);
       int rows = bookService.insertBook(book);
        //主要就是这里进行图书库存的增加,获取book然后得到id，设置自己的
        Book book1 = bookService.selectBookByBook(book);
        int rows2 = bookService.updateBookStorageId(book1);
            if(rows > 0 && row > 0 && rows2 > 0){
            return "OK";
        }else{
            return "FAIL";
        }
    }

    //图书管理方法，参数method为行为
    public boolean changeBook(String method,HttpSession session,Book book){
        Manager manager = (Manager) session.getAttribute(SessionString.Manager_SESSION);
        ChangeBook changeBook = new ChangeBook();
        changeBook.setBook_id(book.getId());
        changeBook.setManager_id(manager.getId());
        //时间
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        changeBook.setChange_time(timestamp);
        //情况更改
        changeBook.setSituation(new BookChangeStuation().toStuation(book,method));
        int rows_change = changeBookService.insertChange(changeBook);
        return rows_change > 0;
    }
}
