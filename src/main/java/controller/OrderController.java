package controller;

import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import po.*;
import service.DeliveryService;
import service.OrderService;
import service.StorageService;
import utils.SessionString;
import utils.TimeStampFactory;

import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
public class OrderController {
    @Autowired
    private OrderService orderService;//订单
    @Autowired
    private DeliveryService deliveryService;//出库
    @Autowired
    private StorageService storageService;//库存修改

    @RequestMapping("/newOrder")
    @ResponseBody
    @Transactional
    public String addNewOrder(Delivery delivery, HttpSession session){
        //首先出库，然后在变成订单
        //时间
        delivery.setTime_delivery(TimeStampFactory.getSystemTime());
        int row = deliveryService.insertDelivery(delivery);
        //库存减少
        Storage storageOrigin = storageService.findStorageById(delivery.getStorage().getId());
        int booknum = storageOrigin.getNum();
        storageOrigin.setNum(booknum-delivery.getNum());
        int row3 = storageService.updateStorageNum(storageOrigin);
        //再读取该delivery然后将读取出来
        Delivery delivery1 = deliveryService.selectDelivery(delivery);
        //添加一个订单
        Order order = new Order();
        order.setDelivery(delivery1);
        //获取user
        User user = (User) session.getAttribute(SessionString.USER_SESSION);
        order.setUser(user);
        //新建一个订单
        int row2 = orderService.insertOneOrder(order);
        if(row > 0 && row2 > 0 && row3 > 0){
            return "OK";
        }else{
            return "FAIL";
        }
    }

    //进行读取所有的订单，展示到前端。
    @RequestMapping("orderList")
    public String getAllOrder(@RequestParam Map<String,Object> map, HttpSession httpSession){
        //注意两个分页的必要参数，page和limit
        List<Map<String,Object>> list = orderService.selectAllOrder(map);
        PageInfo<Map<String ,Object>> pageInfo = new PageInfo<Map<String,Object>>();
        map.put("list",list);
        //设置page和limit
        map.put("limit",0);
        map.put("page",0);
        //聚合数目最大值
        map.put("total",pageInfo.getTotal());
        httpSession.setAttribute(SessionString.ORDER_LIST,list);
        //通过Session进行判定，如果是user那就跳转到user的界面，反之就是manager的界面
        Manager manager = (Manager) httpSession.getAttribute(SessionString.Manager_SESSION);
        if(manager != null){
            return "/managerJsp/managerOrder";
        }else{
            return "/userJsp/userOrder";
        }
    }

    //删除订单
    @RequestMapping("order/delete")
    @ResponseBody
    public String deleteOrder(Integer id){
        int rows = orderService.deleteOrder(id);
        if(rows > 0 ){
            return "OK";
        }else{
            return "FAIL";
        }
    }

    //处理订单
    @RequestMapping("order/update")
    @ResponseBody
    public String updateOrder(Integer id,HttpSession session){
        Manager manager = (Manager) session.getAttribute(SessionString.Manager_SESSION);
        Order order = new Order();
        order.setId(id);
        order.setManager(manager);
        order.setTime_order(TimeStampFactory.getSystemTime());
        int rows = orderService.updateOrderAll(order);
        if(rows > 0 ){
            return "OK";
        }else{
            return "FAIL";
        }
    }

    //查询部分的订单
    @RequestMapping("order/query")
    public String queryOrder(@RequestParam Map<String,Object> map, HttpSession httpSession){
        //注意两个分页的必要参数，page和limit
        List<Map<String,Object>> list = orderService.selectOrderPageWhere(map);
        PageInfo<Map<String ,Object>> pageInfo = new PageInfo<Map<String,Object>>();
        map.put("list",list);
        //聚合数目最大值
        map.put("total",pageInfo.getTotal());
        httpSession.setAttribute(SessionString.BOOK_LIST,list);
        //通过Session进行判定，如果是user那就跳转到user的界面，反之就是manager的界面
        Manager manager = (Manager) httpSession.getAttribute(SessionString.Manager_SESSION);
        if(manager != null){
            return "/managerJsp/managerOrder";
        }else{
            return "/userJsp/userOrder";
        }
    }
}
