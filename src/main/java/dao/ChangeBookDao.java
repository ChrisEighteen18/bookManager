package dao;

import po.ChangeBook;

public interface ChangeBookDao {
    //对于图书的操作无非就是增删改，查就不需要存储了，对于这些操作直接进行加入即可
    public int insertChange(ChangeBook changeBook);
}
