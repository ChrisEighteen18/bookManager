package dao;

import po.Book;

import java.util.List;
import java.util.Map;

public interface  BookDao {
    //查询所有的图书
    public List<Map<String,Object>> selectAllBook(Map<String ,Object>map);
    //通过一些模糊数据进行图书查询
    public List<Map<String,Object>> selectBookPageWhere(Map<String ,Object>map);
    //通过id来查找单个的图书信息
    public Book selectBookById(int id);
    //更新图书信息
    public int updateBook(Book book);
    //通过id删除图书
    public int deleteBook(int id);
    //增加图书
    public int insertBook(Book book);
    //通过book查询id
    public Book selectBookByBook(Book book);
    //更新storageID
    public int updateBookStorageId(Book book);
}
