package dao;

import org.apache.ibatis.annotations.Param;
import po.Manager;

public interface ManagerDao {
    //查询是否由该管理者
    public Manager findManagerByNameAndPassword(@Param("manager_name") String manager_name,@Param("manager_password")String manager_password);
}
