package dao;

import org.apache.ibatis.annotations.Param;
import po.User;

import java.util.List;

public interface UserDao {
    //读取数据库中所有的User
    public List<User> findAllUser();
    //通过用户和密码来查询User
    public User findUserByNameAndPassword(@Param("username") String username, @Param("password") String password);
    //添加一个新用户
    public int addNewUser(User user);
}
