package po;

import java.sql.Timestamp;

public class Delivery {
    private int id;
    private int num;
    private Storage storage;
    private Timestamp time_delivery;

    @Override
    public String toString() {
        return "Delivery{" +
                "id=" + id +
                ", num=" + num +
                ", storage=" + storage +
                ", time_delivery=" + time_delivery +
                '}';
    }

    public Timestamp getTime_delivery() {
        return time_delivery;
    }

    public void setTime_delivery(Timestamp time_delivery) {
        this.time_delivery = time_delivery;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public Storage getStorage() {
        return storage;
    }

    public void setStorage(Storage storage) {
        this.storage = storage;
    }
}
