package po;

public class Manager {
    private int id;
    private String manager_name;
    private String manager_password;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getManager_name() {
        return manager_name;
    }

    public void setManager_name(String manager_name) {
        this.manager_name = manager_name;
    }

    public String getManager_password() {
        return manager_password;
    }

    public void setManager_password(String manager_password) {
        this.manager_password = manager_password;
    }

    @Override
    public String toString() {
        return "Manager{" +
                "id=" + id +
                ", manager_name='" + manager_name + '\'' +
                ", manager_password='" + manager_password + '\'' +
                '}';
    }
}
