package po;

import java.sql.Timestamp;

public class ChangeBook {
    private int id;
    private int manager_id;
    private int book_id;
    private Timestamp change_time;
    private String situation;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getManager_id() {
        return manager_id;
    }

    public void setManager_id(int manager_id) {
        this.manager_id = manager_id;
    }

    public int getBook_id() {
        return book_id;
    }

    public void setBook_id(int book_id) {
        this.book_id = book_id;
    }

    public Timestamp getChange_time() {
        return change_time;
    }

    public void setChange_time(Timestamp change_time) {
        this.change_time = change_time;
    }

    public String getSituation() {
        return situation;
    }

    public void setSituation(String situation) {
        this.situation = situation;
    }
}
