package po;

import java.sql.Timestamp;
import java.util.List;

public class Order {
    private int id;
    private User user;
    private Manager manager;
    private Delivery delivery;
    private Timestamp time_order;
    private Book book;

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    private int situation;//0代表没有处理，1代表已经处理了

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Manager getManager() {
        return manager;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }

    public Delivery getDelivery() {
        return delivery;
    }

    public void setDelivery(Delivery delivery) {
        this.delivery = delivery;
    }

    public Timestamp getTime_order() {
        return time_order;
    }

    public void setTime_order(Timestamp time_order) {
        this.time_order = time_order;
    }

    public int getSituation() {
        return situation;
    }

    public void setSituation(int situation) {
        this.situation = situation;
    }
}
