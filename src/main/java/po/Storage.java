package po;

public class Storage {
    private int id;
    private int num;

    @Override
    public String toString() {
        return "Storage{" +
                "id=" + id +
                ", num=" + num +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
