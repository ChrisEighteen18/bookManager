create database bookManager;
/*
    根据之前创建的ER可知要创建九个表
    首先确定好表的名称再确定好每一列名
    1，book 图书
    2，manger 管理者
    3，user 普通用户
    4，storage 库存
    5，delivery 出库
    6，warehousing 入库
    7，order 订单 （管理者，普通用户，出库的多对多关系）
    8，changeWarehousing 入库更动表（管理者，入库的多对多关系）
    9，changeBook 图书更动表（管理者，图书的多对多关系）
 */
 use bookManager;
/*
    创建库存表
    1，id
    2，num 数目
 */
create table t_storage(
    id int(32) primary key NOT NULL AUTO_INCREMENT,
    num int NOT NULL
)ENGINE = InnoDB;
/*
    确定好book表中的列名和相应的属性
    1,id 图书唯一标识码
    2，author 作者
    3，book_name 书名
    4，price 单价
    5，press 出版社
    6，brief_introduction 简介
    7，serial_number 序列号
    8，img 图片 采用路径保存获取文件
    9，storage_id 库存id（外键）
 */
create table t_book (
    id int(32) primary key NOT NULL AUTO_INCREMENT,
    book_name varchar(30) NOT NULL,
    author varchar(30) NOT NULL,
    price double NOT NULL,
    press varchar(40) NOT NULL,
    brief_introduction varchar(255),
    serial_number varchar(255),
    img varchar(255),
    storage_id int(32),
    foreign key (storage_id) references t_storage(id)
)ENGINE = InnoDB;
/*
    创建管理者表
    1，id
    2,manager_name 管理名
    3，mangaer_password 密码
 */
create table t_manager(
    id int(32) primary key NOT NULL AUTO_INCREMENT,
    manager_name varchar(30) NOT NULL,
    manager_password varchar(30) NOT NULL
)ENGINE = InnoDB;
/*
    创建普通用户表
    1,id
    2,user_name 用户名
    3,user_password 用户密码
 */
 create table t_user(
    id int(32) primary key NOT NULL AUTO_INCREMENT,
    user_name varchar(30) NOT NULL,
    user_password varchar(30) NOT NULL
)ENGINE = InnoDB;

/*
    创建出库表
    1，id
    2，num 数目
    3，time_delivery 时间
    4，storage_id 库存id
 */
create table t_delivery(
    id int(32) primary key NOT NULL AUTO_INCREMENT,
    num int NOT NULL,
    time_delivery DATETIME,
    storage_id int,
    foreign key (storage_id) references t_storage(id)
)ENGINE = InnoDB;
/*
    入库表
    1，id
    2，num 数目
    3，storage_id 库存id
 */
 create table t_warehousing(
    id int(32) primary key NOT NULL AUTO_INCREMENT,
    num int NOT NULL,
    storage_id int,
    foreign key (storage_id) references t_storage(id)
)ENGINE = InnoDB;
/*
    7，order 订单 （管理者，普通用户，出库的多对多关系）
    1,id
    2,user_id 普通用户
    3,manager_id 管理者
    4，delivery_id 出库id 2-4外键
    5，time_order 下单时间
    6，situation 订单情况（数字0，1表示，管理者发货与否）
 */
  create table t_order(
    id int(32) primary key NOT NULL AUTO_INCREMENT,
    user_id int NOT NULL,
    manager_id int,
    delivery_id int NOT NULL,
    time_order DateTime,
    situation int,
    foreign key (user_id) references t_user(id),
    foreign key (manager_id) references t_manager(id),
    foreign key (delivery_id) references t_delivery(id)
)ENGINE = InnoDB;
/*
    8，changeWarehousing 入库更动表（管理者，入库的多对多关系）
    1,id
    2,manager_id 管理者
    3，warehousing_id 入库id 2-3外键
    4，situation 订单情况
    5，change_time 时间
 */
 create table t_changeWarehousing(
    id int(32) primary key NOT NULL AUTO_INCREMENT,
    manager_id int NOT NULL,
    warehousing_id int ,
    change_time DateTime NOT NULL,
    situation varchar(255),
    foreign key (manager_id) refer  ences t_manager(id),
    foreign key (warehousing_id) references t_warehousing(id)
)ENGINE = InnoDB;
/*
    9，changeBook 图书更动表（管理者，图书的多对多关系）
    1,id
    2,manager_id 管理者
    3，book_id 图书id 2-3外键
    4，situation 订单情况
    5，change_time 时间
 */
 create table t_changeBook(
    id int(32) primary key NOT NULL AUTO_INCREMENT,
    manager_id int NOT NULL,
    book_id int ,
    change_time DateTime NOT NULL,
        situation varchar(255),
    foreign key (manager_id) references t_manager(id),
    foreign key (book_id) references t_book(id)
)ENGINE = InnoDB;